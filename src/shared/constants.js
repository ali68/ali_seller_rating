module.exports = {
    SHOW_PAGE_ACTION: 'SHOW_PAGE_ACTION',
    QUERY_SCORE: 'QUERY_SCORE',
    SKS: ['mimuRrf', 'UrNZRbY', 'uvrVJEu', 'j2rFimM', 'FY3naIE', 'N7eUfIu'],
    DEFAULT_SETTINGS: {},
    UPDATE_SETTINGS_TO_TAB: 'UPDATE_SETTINGS_TO_TAB',
    IS_DEVELOPMENT: process.env.NODE_ENV === "development",
    SERVER_URL: process.env.NODE_ENV === "development" ? 'http://localhost:5000' : 'http://alislr.info'
}