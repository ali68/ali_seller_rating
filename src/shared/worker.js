onmessage = function(e){
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        if(request.readyState == 4 && request.status < 400){
            postMessage(request.response);
        }
    }
    request.open(e.data.method, e.data.uri);
    if(e.data.type && e.data.type === 'json'){
        request.setRequestHeader("Content-Type", "application/json");
    }

    if(e.data.data){
        request.send(JSON.stringify(e.data.data))
    }else{
        request.send();
    }
}