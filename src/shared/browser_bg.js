import {denodeify} from './denodeify'
import {storage, runtime, load} from './browser'
import pThrottle from 'p-throttle'
import _rnd from 'lodash/random'

const RSVP = require('rsvp')
const cookies = {
    remove: denodeify(chrome.cookies.remove)
}

// const downloads = {
//     download: denodeify(chrome.downloads.download),
//     show: denodeify(chrome.downloads.show),
// }


const tabs = {
    create: denodeify(chrome.tabs.create),
    update: denodeify(chrome.tabs.update),
    sendMessage: denodeify(chrome.tabs.sendMessage)
}

const rate = {
    send: pThrottle(function (options) {
        return myWebWorker(options)
    }, _rnd(2.1, 3.1), 1000),
    request: pThrottle(function (options) {
        return myWebWorker(options)
    }, 10, 1000)
}

const myWebWorker = (options) => {
    const promise = new RSVP.Promise((resolve, reject) => {
        try {
            let request = new XMLHttpRequest();
            request.withCredentials = true;
            request.onreadystatechange = function(){
                if(request.readyState == 4 && request.status < 400){
                    resolve(request.response);
                }
            }
            request.onload = function () {
               if(options.uri !== request.responseURL && request.responseURL.indexOf('alisec') === -1){
                   request.open(options.method, request.responseURL);
               }
            }
            request.open(options.method, options.uri);
            // request.setRequestHeader("Content-Type", "text/html");
            if(options.type && options.type === 'json'){
                request.setRequestHeader("Content-Type", "application/json");
            }

            if(options.data){
                request.send(JSON.stringify(options.data))
            }else{
                request.send();
            }
        } catch (ex) {
            reject(ex)
        }
    })
    return promise
}


const webWorker = {
    send: rate.send,
    request: rate.request,
}

export {storage, runtime, cookies, tabs, webWorker, load}