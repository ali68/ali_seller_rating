import {runtime} from '../shared/browser'
import C from '../shared/constants'
import _uniq from 'lodash/uniq'
import _find from 'lodash/find'
import './index.css'
import toastr from 'toastr'
import 'toastr/build/toastr.css'
let settings = window.localStorage.getItem('_s3tt1ngs_') ? JSON.parse(window.localStorage.getItem('_s3tt1ngs_')) : undefined
const currentURL = document.location.href;
runtime.sendMessage({
    action: C.SHOW_PAGE_ACTION,
    data: null
}).then((res) => {
    if (res && res['settings']) {
        window.localStorage.setItem('_s3tt1ngs_', JSON.stringify(res['settings']))
    }
});

String.prototype.contains = function(items){
    const self = this;
    return _find(items, (i) => {return self.indexOf(i)});
};

const mustSignInMessage = chrome.i18n.getMessage('mustSignInMessage');
const extName = chrome.i18n.getMessage('extName');
const lbl_feedback_score = chrome.i18n.getMessage('lbl_feedback_score');

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": 0,
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

$(document).ready(() => {
    try {
        const isSigned = is_signed();
        if(!isSigned && currentURL.indexOf('aliexpress.com') !== -1 && currentURL.indexOf('portals.aliexpress.com') === -1){
            toastr.error(mustSignInMessage, extName);
        }
        if(isSigned){
            if(currentURL.indexOf('/category/') !== -1){
                const store_list = $('a.store')
                let store_ids = []
                store_list.map((i, e) => {
                    if(e){
                        const storeId = get_store_id(e.getAttribute('href'))
                        if (!isNaN(storeId)) {
                            store_ids.push(storeId)
                            store_ids = _uniq(store_ids)
                            const rateTag = create_rate_tag(storeId);
                            e.parentNode.appendChild(rateTag);
                        }
                    }
                });
                store_ids.forEach((storeId) => {
                    send_query_score(storeId)
                })
            }

            const detailPages = [
                'aliexpress.com/item/',
                'aliexpress.com/store/product/',
                'aliexpress.com/store/feedback-score/'
            ]
            if(currentURL.contains(detailPages)){
                const pbTag = document.querySelector('.positive-feedback');
                const idTag = document.querySelector('.store-header-container .shop-name a');
                const storeId = idTag ? get_store_id(idTag.getAttribute('href')) : 'xxx';
                if(!isNaN(storeId) && pbTag){
                    const rateTag = create_rate_tag(storeId, true);
                    const pNode = pbTag.parentNode;
                    pNode.insertBefore(rateTag, pbTag);
                    send_query_score(storeId, true);
                }
            }
        }

    } catch (ex) {
        if(C.IS_DEVELOPMENT){
            console.error(ex)
        }
    }
})

function is_signed() {
    const accountTag = document.querySelector('.account-name')
    let rs = false
    if(accountTag){
        rs = accountTag.innerText.trim().length > 0
    }
    return rs;
}

function get_store_id(href) {
    href = href.split('?')[0];
    let storeId = 'xxx';
    if (href.match(/store\/\d+/)) {
        storeId = href.split('/').pop()
    }
    return storeId;
}

function create_rate_tag(storeId, isShowFbScore) {
    isShowFbScore = isShowFbScore | false;
    const divTag = document.createElement('div');
    const aTag = document.createElement('a');
    const imgTag = document.createElement('img');
    if(isShowFbScore){
        const spTag = document.createElement('span');
        aTag.appendChild(spTag);
        spTag.setAttribute('class', `fb_score_${storeId} fb_score`);
    }

    aTag.appendChild(imgTag);
    divTag.appendChild(aTag);
    divTag.setAttribute('class', 'store_feedback_rating');
    imgTag.setAttribute('src', chrome.runtime.getURL(`icons/level/loading.gif`));
    imgTag.setAttribute('class', `rate_${storeId}`);
    aTag.setAttribute('class', `store_url_${storeId}`);
    aTag.setAttribute('href', 'javascript:void(0)');
    return divTag
}

function send_query_score(storeId, isFull) {
    const storeKey = `store_${storeId}`
    if(!sessionStorage[storeKey]){
        runtime.sendMessage({
            action: C.QUERY_SCORE,
            data: {
                storeId: storeId,
                isFull: isFull || false
            }
        }).then((res) => {
            res = JSON.stringify(res);
            if(res !== undefined){
                sessionStorage[storeKey] = res;
                showRating(storeKey);
            }
        })
    }else{
        showRating(storeKey);
    }

}

function showRating(storeKey) {
    const store = JSON.parse(sessionStorage[storeKey]);
    if(store && store.storeNum){
        document.querySelectorAll(`img.rate_${store.storeNum}`).forEach((el) => {
            if(store.sellerLevel !== ""){
                const aTag = el.parentNode;
                aTag.setAttribute('href', `https://www.aliexpress.com/store/feedback-score/${store.storeNum}.html`);
                const fbTag = aTag.querySelector(`span.fb_score_${store.storeNum}`);
                if(fbTag && store.totalNum){
                    fbTag.textContent = store.totalNum;
                }
                el.setAttribute('src', chrome.runtime.getURL(`icons/level/${store.sellerLevel}.gif`));
                el.setAttribute('title', `${lbl_feedback_score} ${store.totalNum}`);
            }else{
                el.remove()
            }
        })
    }
}
