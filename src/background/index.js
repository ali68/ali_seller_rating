import {webWorker} from '../shared/browser_bg'
import C from '../shared/constants'
import RSVP from 'rsvp'

import _template from 'lodash/template'
import _uniq from 'lodash/uniq'
import _pick from 'lodash/pick'

import './util'

let tabIds = []

chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    const {action, data} = msg
    const senderTabId = sender.tab.id
    switch (action) {
        case C.SHOW_PAGE_ACTION:
            chrome.pageAction.show(senderTabId)
            _upsert(senderTabId);
            // remove_expired_store_in_storage().then((res) => {
            //     console.log(`Removed ${res} stores!`)
            // });
            break;
        case C.QUERY_SCORE:
            if (data.storeId) {
                get_score(data.storeId).then((store) => {
                    if (data.isFull) {
                        sendResponse(store)
                    } else {
                        sendResponse(_pick(store, ['storeNum', 'sellerLevel', 'adminSeq', 'totalNum', 'feedbackRating']))
                    }
                })
            }
            break;
    }
    return true
})

function get_score(storeId) {
    return new RSVP.Promise((resolve, reject) => {
        try {
            webWorker.request({
                uri: `${C.SERVER_URL}/store/${storeId}`,
                method: 'GET'
            }).then((res) => {
                res = JSON.parse(res);
                if (res && res.storeNum) {
                    // save_store_to_storage(res)
                    resolve(res);
                } else {
                    query_score(storeId).then((res) => {
                        if (res && res.storeNum) {
                            // save_store_to_storage(res)
                            resolve(res);
                        }
                    })
                }
            })
        } catch (ex) {
            console.error(ex);
            reject(ex);
        }
    })

}

function query_score(storeId) {
    return new RSVP.Promise((resolve, reject) => {
        try {
            const store_dt_tpl = _template('https://www.aliexpress.com/store/feedback-score/<%=storeId%>.html')
            const seller_info_tpl = _template('https://m.aliexpress.com/store/sellerInfo.htm?sellerAdminSeq=<%=sellerId%>')
            const option = {
                method: 'GET',
                uri: store_dt_tpl({storeId: storeId})
            }
            webWorker.send(option).then((res) => {
                const parser = new DOMParser();
                const HTML = parser.parseFromString(res, 'text/html')
                const aTag = HTML.querySelector('a[data-memberid][data-id1]')
                let adminSeqId = 'xxx'
                if (aTag) {
                    adminSeqId = aTag.getAttribute('data-id1')
                } else {
                    const ownerMemberId = res.match(/ownerMemberId\: \'(.*)\'\,/)
                    adminSeqId = ownerMemberId ? ownerMemberId[1].toString() : 'xxx'
                }
                if (!isNaN(adminSeqId)) {
                    option['uri'] = seller_info_tpl({sellerId: adminSeqId})
                    webWorker.send(option).then((res1) => {
                        const sellerInfoDTO_str = res1.match(/sellerInfoDTO \= (.*)\;/)
                        let store = sellerInfoDTO_str ? JSON.parse(sellerInfoDTO_str[1]) : null;
                        if (store) {
                            if (store.storeNum) {
                                webWorker.send({
                                    data: store,
                                    uri: `${C.SERVER_URL}/store`,
                                    method: 'POST'
                                }).then((res) => {
                                    res = JSON.parse(res);
                                    if (res && res.storeNum) {
                                        // let expiredAt = new Date();
                                        // res['expiredAt'] = expiredAt.setMonth(expiredAt.getMonth() - 2);
                                        resolve(res);
                                    }
                                })
                            }
                        }
                    })
                }
            })
        } catch (ex) {
            reject(ex)
        }
    })
}

// function save_store_to_storage(store) {
//     if (store && store.storeNum) {
//         storage.local.set({[`store_${store.storeNum}`]: JSON.stringify(store)})
//     }
// }

// function remove_expired_store_in_storage() {
//     return new RSVP.Promise((resolve, reject) => {
//         try {
//             storage.sync.get(['__CACHED_REMOVED_AT']).then((data) => {
//                 const __CACHED_REMOVED_AT = data['__CACHED_REMOVED_AT'];
//                 if(__CACHED_REMOVED_AT === undefined){
//                     storage.sync.set({'__CACHED_REMOVED_AT': Date.now()}).then(() => {
//                         resolve(0);
//                     });
//                 }else{
//                     const _12hours = 1000 * 60 * 60 * 12
//                     const _now = Date.now()
//                     const is_must_remove = (_now - __CACHED_REMOVED_AT) / _12hours <= 0;
//                     if(is_must_remove){
//                         storage.local.get(null).then((all) => {
//                             const CACHE_DURATION = 2592000 * 1000;
//                             const cutoff = Date.now() - CACHE_DURATION;
//                             const expiredKeys = Object.keys(all).filter(k => {
//                                 if(k.indexOf('store_') !== -1){
//                                     const store = JSON.parse(all[k]);
//                                     return store && store['expiredAt'] < cutoff
//                                 }
//                                 return false;
//                             });
//                             storage.sync.set({'__CACHED_REMOVED_AT': Date.now()});
//                             if (expiredKeys.length > 0) {
//                                 storage.local.remove(expiredKeys).then((res) => {
//                                     resolve(expiredKeys.length || 0)
//                                 });
//                             } else {
//                                 resolve(0);
//                             }
//                         })
//                     }else{
//                         resolve(0)
//                     }
//                 }
//
//             })
//         } catch (ex) {
//             reject(ex)
//         }
//     })
// }

function _upsert(newVal) {
    tabIds.push(newVal)
    tabIds = _uniq(tabIds)
}
