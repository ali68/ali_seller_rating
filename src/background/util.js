import {load, cookies, tabs} from '../shared/browser_bg'
import C from '../shared/constants'
import _template from 'lodash/template'
const promotion_uri_tpl = _template('http://s.click.aliexpress.com/deep_link.htm?aff_short_key=mimuRrf&dl_target_url=<%=uri%>')
let service, tracker;
const _AnalyticsCode = 'UA-74453743-13';

load.script(chrome.runtime.getURL('shared/google-analytics-bundle.js'), function () {
    console.info('google analytics platform loaded...');
    service = analytics.getService('aliexpress_seller_ratings');
    tracker = service.getTracker(_AnalyticsCode);
    tracker.sendAppView('App view');
});

function check_session_valid(sessionName, _hours, cb) {
    const _1hour = 1000 * 60 * 60
    const _15minutes = _1hour / 4
    let is_valid = false
    chrome.storage.sync.get([sessionName], function (result) {
        const _now = Date.now()
        let __SESSI0N_ = result[sessionName]
        if (!__SESSI0N_) {
            is_valid = true
        } else {
            is_valid = ((_now - __SESSI0N_ - _15minutes) / _1hour) >= _hours
        }
        cb(is_valid, sessionName, _now)
    })
}
function openPromotion(tabId, promotionUrl, requestUrl, label){
    cookies.remove({
        url: "http://aliexpress.com",
        name: "aep_usuc_f"
    }).then(() => {
        return tabs.update(tabId, {
            url: promotionUrl,
            active: true
        })
    }).then(() => {
        if (tracker) {
            tracker.sendEvent('Chrome', label, requestUrl)
        }
    })
}
function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
chrome.webRequest.onCompleted.addListener(function (detail) {
    try {
        if (detail.frameId >= 0 && detail.tabId >= 0 && detail.type === 'main_frame') {
            const requestUrl = detail.url.split('?')[0] || detail.url
            const _href = promotion_uri_tpl({uri: encodeURI(requestUrl)})
            const sk = getParameterByName('sk', detail.url)
            // console.log(sk)
            if(sk !== null && C.SKS.indexOf(sk) === -1){
                chrome.storage.sync.set({
                    __SESSI0N_: Date.now()
                })
                openPromotion(detail.tabId, _href, requestUrl, 'DeepLink_Override')
            }else {
                if(detail.url.match(/\/product\/|\/item\//) !== null){
                    check_session_valid('__SESSI0N_', 1, function (is_valid, sessionName, _now) {
                        if (is_valid) {
                            chrome.storage.sync.set({
                                [sessionName]: _now
                            })
                            openPromotion(detail.tabId, _href, requestUrl, 'DeepLink')
                        }
                    })
                }
            }

        }
    } catch (ex) {

    }
}, {urls: ['*://*.aliexpress.com/*']})